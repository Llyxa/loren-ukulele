<?php

use App\Http\Controllers\JenisController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\WarnaController;
use App\Models\Produk;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('/');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('/home');

//Start Produk
Route::resource('produk',\App\Http\Controllers\ProdukController::class);

// Route::get('produk-detail/', [\App\Http\Controllers\ProdukDetailController::class, 'index'])->name('produk-detail.index');
// Route::get('/produk-detail/create/{id}', [\App\Http\Controllers\ProdukDetailController::class, 'create'])->name('produk-detail.create');
// });
// Route::post('produk-detail/store', [\App\Http\Controllers\ProdukDetailController::class, 'store'])->name('produk-detail.store');

Route::resource('produk-detail',\App\Http\Controllers\ProdukDetailController::class);

//Start Master
Route::resource('jenis',\App\Http\Controllers\JenisController::class);
Route::resource('warna',\App\Http\Controllers\WarnaController::class);
