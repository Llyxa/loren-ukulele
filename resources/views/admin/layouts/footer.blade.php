<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-md-left d-block d-md-inline-block mt-25 ml-0">Ukulele Airen</span><span class="float-md-right d-none d-md-block">Your Music Partner<i data-feather="music"></i></span></p>
</footer>
<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>